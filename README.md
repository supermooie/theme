# Vikunja Theme

[![Build Status](https://drone.kolaente.de/api/badges/vikunja/theme/status.svg)](https://drone.kolaente.de/vikunja/theme)

This repo provides the [hugo](https://gohugo.io) theme for the [vikunj.io](https://vikunj.io) website, docs and blog.

## Building

You need nodejs 16 and yarn v1.

```bash
yarn # install all dependencies
yarn prod
yarn clean
yarn release
```

Will give you the theme in a tar file in the `dist/` folder.

